# Project supermarket checkout
Our supermarket 's quest for global reach has prompted us to open a new supermarket.

We sell only the following three products

```
Product code    | Name		| Price
----------------------------------------
FR1	        | Tea           | 3.11 ‎€
SR1	        | Strawberries  | 5.00 ‎€
CF1	        | Coffee        | 11.23 ‎€
```
- The CEO is a big fan of buy-one-get-one-free for the Tea. He wants a rule to do this.

- The COO though likes low prices and wants people buying strawberries to get a price discount for bulk purchaces. If you buy 3 or more strawberries, the price should drop to 4.50€.

Our checkout processes scan items of an order and because the CEO and COO change their minds often, it needs to be flexible regarding the our pricing rules.

The interface of our checkout proccess looks like this:

```
<?php

$co = new Checkout($pricingRules);
$co->scan($item);
$co->scan($item);
$price = $co->total();
```
Implement a checkout system that fulfils these requirements

### Test data
```
Cart: FR1,SR1,FR1,FR1,CF1
Total price expected: 22.45€

Cart: FR1,FR1
Total price expected: 3.11€

Cart: SR1,SR1,FR1,SR1
Total price expected: 16.61€
```
